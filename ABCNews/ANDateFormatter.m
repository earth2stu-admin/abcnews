//
//  ANDateFormatter.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "ANDateFormatter.h"

static NSDateFormatter *_jsonFormatter;
static NSDateFormatter *_cellFormatter;

@implementation ANDateFormatter

+ (ANDateFormatter *)shared {
    static ANDateFormatter *_shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[self alloc] init];
    });
    return _shared;
}

- (NSDateFormatter *)jsonFormatter {
    if (!_jsonFormatter) {
        _jsonFormatter = [[NSDateFormatter alloc] init];
        _jsonFormatter.dateFormat = @"EEE, d MMM yyyy HH:mm:ss Z";
    }
    return _jsonFormatter;
}

- (NSDateFormatter *)cellFormatter {
    if (!_cellFormatter) {
        _cellFormatter = [[NSDateFormatter alloc] init];
        _cellFormatter.dateFormat = @"MMM d, yyyy hh:mm a";
    }
    return _cellFormatter;
}

@end
