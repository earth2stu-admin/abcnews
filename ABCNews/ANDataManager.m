//
//  ANDataManager.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "ANDataManager.h"
#import "ANArticle.h"

@implementation ANDataManager

+ (ANDataManager*)sharedManager {
    static ANDataManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    return _sharedManager;
}

- (void)loadArticlesForced:(BOOL)forced withCompletion:(void (^)(NSArray *))completion {
    NSURL *newsURL = [NSURL URLWithString:kANBaseURL];
    NSData *fileData = [NSData dataWithContentsOfURL:[self articleCacheURL]];
    if (!forced) {
        // check for cached data
        if (fileData) {
            // return cache if found
            NSArray *articles = [self articlesWithData:fileData];
            completion(articles);
            return;
        }
    }

    NSURLRequest *request = [NSURLRequest requestWithURL:newsURL];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            // cache the data
            [data writeToURL:[self articleCacheURL] atomically:YES];
            NSArray *articles = [self articlesWithData:data];
            // call on main thread cos we are in the background
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(articles);
            });
        } else {
            if (fileData) {
                NSArray *articles = [self articlesWithData:fileData];
                // call on main thread cos we are in the background
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(articles);
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil);
                });
            }
        }
    }] resume];
}

// parse json data into model objects
// returns nil if an error occured
- (NSArray*)articlesWithData:(NSData*)data {
    NSError *jsonError = nil;
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
    if (!jsonError) {
        // browse the json object
        NSDictionary *rootObject = [jsonObject objectForKey:kANRootObject];
        NSDictionary *feedObject = [rootObject objectForKey:kANFeedObject];
        NSArray *entries = [feedObject objectForKey:kANEntriesArray];
        NSMutableArray *articles = [NSMutableArray array];
        // add all the articles
        for (NSDictionary *entryDict in entries) {
            ANArticle *article = [[ANArticle alloc] initWithDictionary:entryDict];
            [articles addObject:article];
        }
        return [NSArray arrayWithArray:articles];
    } else {
        return nil;
    }
}

- (NSURL*)articleCacheURL {
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [cachesPath stringByAppendingPathComponent:@"articles.json"];
    return [NSURL fileURLWithPath:filePath];
}

@end
