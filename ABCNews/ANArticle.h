//
//  ANArticle.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANArticle : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *thumbnailURL;
@property (nonatomic, strong) NSString *featuredURL;


- (id)initWithDictionary:(NSDictionary*)dict;

@end
