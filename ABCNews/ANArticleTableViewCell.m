//
//  ANArticleTableViewCell.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "ANArticleTableViewCell.h"
#import "UIColor+ABCNews.h"
#import "UIFont+ABCNews.h"
#import "ANDateFormatter.h"
#import "UIImageView+Caching.h"

@interface ANArticleTableViewCell() {
    UIImageView *thumbnailImageView;
    UITextView *titleTextView;
    UILabel *dateLabel;
    NSLayoutConstraint *textViewTopConstraint;  // how far the text view is from the top of the cell
    NSLayoutConstraint *textViewRightConstraint; // how far the right edge of the text view is from the right edge of cell
    NSLayoutConstraint *imageViewWidthConstraint; // width of the image - use to hide in standard articles
    ANArticle *article;
}

@end

@implementation ANArticleTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.separatorInset = UIEdgeInsetsZero;
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        
        // setup text view
        titleTextView = [[UITextView alloc] init];
        titleTextView.textContainer.lineFragmentPadding = 0;
        titleTextView.textContainerInset = UIEdgeInsetsZero;
        titleTextView.userInteractionEnabled = NO;
        titleTextView.translatesAutoresizingMaskIntoConstraints = NO;
        titleTextView.textContainer.maximumNumberOfLines = 3;
        titleTextView.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.contentView addSubview:titleTextView];
        
        // setup image view
        thumbnailImageView = [[UIImageView alloc] init];
        thumbnailImageView.translatesAutoresizingMaskIntoConstraints = NO;
        thumbnailImageView.clipsToBounds = YES;
        thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:thumbnailImageView];
        
        // setup date label
        dateLabel = [[UILabel alloc] init];
        dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
        dateLabel.textColor = [UIColor dateText];
        dateLabel.font = [UIFont dateText];
        [self.contentView addSubview:dateLabel];
        
        UIImageView *cellDivider = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell_divider"]];
        cellDivider.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:cellDivider];
        
        // divider common constraints
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cellDivider attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cellDivider attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.0]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cellDivider attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
        
        if ([reuseIdentifier isEqualToString:kANArticleIdentifier]) {
            // constraints for standard article
            [self.contentView addConstraint:[NSLayoutConstraint
                                             constraintWithItem:titleTextView
                                             attribute:NSLayoutAttributeTop
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self.contentView
                                             attribute:NSLayoutAttributeTop
                                             multiplier:1.0
                                             constant:15.0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleTextView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:20.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleTextView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:thumbnailImageView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:-20.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleTextView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-20.0]];
            
            imageViewWidthConstraint = [NSLayoutConstraint constraintWithItem:thumbnailImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:120.0];
            [thumbnailImageView addConstraint:imageViewWidthConstraint];
            
            [thumbnailImageView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnailImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:110.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnailImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnailImageView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:19.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:20.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-140.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-9.0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cellDivider attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:thumbnailImageView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
            
            
        } else {
            
            // get notifications for orientation changes
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willRotate:) name:UIApplicationWillChangeStatusBarOrientationNotification object:nil];
            
            
            // constraints for featured article
            textViewTopConstraint = [NSLayoutConstraint
                                     constraintWithItem:titleTextView
                                     attribute:NSLayoutAttributeTop
                                     relatedBy:NSLayoutRelationLessThanOrEqual
                                     toItem:self.contentView
                                     attribute:NSLayoutAttributeTop
                                     multiplier:1.0
                                     constant:200.0];
            [self.contentView addConstraint:textViewTopConstraint];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleTextView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:20.0]];
            textViewRightConstraint = [NSLayoutConstraint constraintWithItem:titleTextView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-20.0];
            [self.contentView addConstraint:textViewRightConstraint];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:titleTextView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-20.0]];
            
            imageViewWidthConstraint = [NSLayoutConstraint constraintWithItem:thumbnailImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:320.0];
            
            [thumbnailImageView addConstraint:imageViewWidthConstraint];
            [thumbnailImageView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnailImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:180.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnailImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:thumbnailImageView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:19.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:20.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-140.0]];
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:dateLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-9.0]];
            
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:cellDivider attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
        }
        
        
        
        
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (void)dealloc {
    // remove notifications when we die
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)willRotate:(NSNotification*)notification {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return;
    }
    if (article.featuredURL) {
        // check the new orientation if there is an image
        UIInterfaceOrientation theOrientation = [[notification.userInfo valueForKey:UIApplicationStatusBarOrientationUserInfoKey] integerValue];
        if (UIInterfaceOrientationIsLandscape(theOrientation)) {
            // move the text to the top
            textViewTopConstraint.constant = 20.0f;
            // push the text to the left of the image
            textViewRightConstraint.constant = -300.0f;
            titleTextView.textContainer.maximumNumberOfLines = 4;
            // update image width for landscape
            imageViewWidthConstraint.constant = 280.0f;
        } else {
            // move the text under the image
            textViewTopConstraint.constant = 200.0f;
            // and full width
            textViewRightConstraint.constant = -20.0f;
            titleTextView.textContainer.maximumNumberOfLines = 3;
            // update image width for portrait
            imageViewWidthConstraint.constant = 320.0f;
        }
    } else {
        // no image so set the text to full width at the top
        textViewTopConstraint.constant = 20.0f;
        textViewRightConstraint.constant = -20.0f;
    }
    
}



- (void)setArticle:(ANArticle *)anArticle {
    article = anArticle;
    
    // setup article text formatting
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    
    if ([self.reuseIdentifier isEqualToString:kANArticleIdentifier]) {
        style.lineSpacing = 2;
        titleTextView.attributedText = [[NSAttributedString alloc]
                                        initWithString:article.title
                                        attributes:@{
                                                     NSParagraphStyleAttributeName : style,
                                                     NSFontAttributeName: [UIFont articleText],
                                                     NSForegroundColorAttributeName: [UIColor articleText]}];
        if (article.thumbnailURL) {
            [thumbnailImageView setImageWithURL:[NSURL URLWithString:article.thumbnailURL] andPlaceholder:[UIImage imageNamed:@"article_thumb"]];
            // got an image so make sure the imageview is showing
            imageViewWidthConstraint.constant = 120.0f;
        } else {
            // no image so hide the imageview
            imageViewWidthConstraint.constant = 0.0f;
        }
        
    } else {
        style.lineSpacing = 3;
        titleTextView.attributedText = [[NSAttributedString alloc]
                                        initWithString:article.title
                                        attributes:@{
                                                     NSParagraphStyleAttributeName : style,
                                                     NSFontAttributeName: [UIFont featuredArticleText],
                                                     NSForegroundColorAttributeName: [UIColor articleText]}];
        if (article.featuredURL) {
            [thumbnailImageView setImageWithURL:[NSURL URLWithString:article.featuredURL] andPlaceholder:[UIImage imageNamed:@"featured_thumb"]];
            // no featured image so move text to the top
            textViewTopConstraint.constant = 200.0f;
            // got an image so make sure the imageview is showing
            imageViewWidthConstraint.constant = 320.0f;
        } else {
            // no featured image so move text to the top
            textViewTopConstraint.constant = 20.0f;
            // no image so hide the imageview
            imageViewWidthConstraint.constant = 0.0f;
        }
        
    }

    dateLabel.text = [[[ANDateFormatter shared] cellFormatter] stringFromDate:article.date];
    
    [self setNeedsUpdateConstraints];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
