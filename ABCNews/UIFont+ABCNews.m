//
//  UIFont+ABCNews.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "UIFont+ABCNews.h"

@implementation UIFont (ABCNews)

+ (UIFont *)featuredArticleText {
    return [UIFont fontWithName:@"Georgia" size:22.0];
}

+ (UIFont *)articleText {
    return [UIFont fontWithName:@"Georgia" size:17.0];
}

+ (UIFont *)dateText {
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:11];
}

@end
