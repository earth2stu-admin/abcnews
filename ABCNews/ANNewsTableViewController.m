//
//  ANNewsTableViewController.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "ANNewsTableViewController.h"
#import "ANDataManager.h"
#import "ANArticle.h"
#import "ANArticleTableViewCell.h"
#import "ANArticleViewController.h"
#import "UIColor+ABCNews.h"

@interface ANNewsTableViewController () {
    NSArray *_articles;
    UIRefreshControl *refreshControl;
}

@end

@implementation ANNewsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // setup tableview
    [self.tableView registerClass:[ANArticleTableViewCell class] forCellReuseIdentifier:kANFeaturedArticleIdentifier];
    [self.tableView registerClass:[ANArticleTableViewCell class] forCellReuseIdentifier:kANArticleIdentifier];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // add refresh control
    refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl = refreshControl;
    refreshControl.tintColor = [UIColor articleText];
    refreshControl.translatesAutoresizingMaskIntoConstraints = NO;
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    // load articles
    [[ANDataManager sharedManager] loadArticlesForced:NO withCompletion:^(NSArray *articles) {
        _articles = articles;
        [self.tableView reloadData];
    }];
    
    // set up nav bar presentation
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    
    [navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar"]
                       forBarPosition:UIBarPositionAny
                           barMetrics:UIBarMetricsDefault];
    
    if ([[UIScreen mainScreen] bounds].size.height == 480.0) {
        [navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar_landscape_35"]
                           forBarPosition:UIBarPositionAny
                               barMetrics:UIBarMetricsLandscapePhone];
    } else {
        [navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar_landscape"]
                           forBarPosition:UIBarPositionAny
                               barMetrics:UIBarMetricsLandscapePhone];
    }
    
    
    
    [navigationBar setShadowImage:[UIImage imageNamed:@"navigation_bar_divider"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _articles.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ANArticleTableViewCell *cell;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:kANFeaturedArticleIdentifier forIndexPath:indexPath];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:kANArticleIdentifier forIndexPath:indexPath];
    }

    ANArticle *article = [_articles objectAtIndex:indexPath.row];
    [cell setArticle:article];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        ANArticle *featuredArticle = [_articles objectAtIndex:0];
        if (featuredArticle.featuredURL) {
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
                return 180.0f;
            } else {
                return 315.0f;
            }
        } else {
            return 110.0f;
        }
    } else {
        return 110.0f;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ANArticle *article = [_articles objectAtIndex:indexPath.row];
    ANArticleViewController *articleController = [[ANArticleViewController alloc] init];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.delegate didSelectArticleWithURL:[NSURL URLWithString:article.link]];
    } else {
        articleController.contentURL = [NSURL URLWithString:article.link];
        [self.navigationController pushViewController:articleController animated:YES];
    }
}


#pragma mark Button Actions

- (void)touchRefresh:(id)sender {
    [[ANDataManager sharedManager] loadArticlesForced:YES withCompletion:^(NSArray *articles) {
        _articles = articles;
        [self.tableView reloadData];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }];
}

#pragma mark Refresh

- (void)refreshTable {
    // load articles
    [[ANDataManager sharedManager] loadArticlesForced:YES withCompletion:^(NSArray *articles) {
        _articles = articles;
        [refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}


@end
