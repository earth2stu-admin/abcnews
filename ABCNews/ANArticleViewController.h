//
//  ANArticleViewController.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANNewsTableViewController.h"


@interface ANArticleViewController : UIViewController <ANNewsTableDelegate, UISplitViewControllerDelegate>

@property (nonatomic, strong) NSURL *contentURL;

@end
