//
//  ANDataManager.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ANDataManager : NSObject

+ (ANDataManager*)sharedManager;

- (void)loadArticlesForced:(BOOL)forced withCompletion:(void (^)(NSArray *articles))completion;

@end
