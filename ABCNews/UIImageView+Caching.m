//
//  UIImageView+Caching.m
//  ABCNews
//
//  Created by Stu on 24/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "UIImageView+Caching.h"

@implementation UIImageView (Caching)

- (void)setImageWithURL:(NSURL *)url andPlaceholder:(UIImage *)placeholderImage {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSString *fileName = [url lastPathComponent];
        NSURL *cacheFileURL = [self imageCacheURLForFile:fileName];
        __block UIImage *cachedImage = [UIImage imageWithContentsOfFile:[cacheFileURL path]];
        if (cachedImage) {
            // found the cached image
            // render it on the background to make sure it is decompressed
            UIGraphicsBeginImageContext(CGSizeMake(100, 100));
            [cachedImage drawAtPoint:CGPointZero];
            UIGraphicsEndImageContext();
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setImage:cachedImage];
            });
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setImage:placeholderImage];
        });

        NSURLSession *session = [NSURLSession sharedSession];
        [[session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSError *copyError;
                [[NSFileManager defaultManager] copyItemAtURL:location toURL:cacheFileURL error:&copyError];
                if (!copyError) {
                    cachedImage = [UIImage imageWithContentsOfFile:[cacheFileURL path]];
                    // render it on the background to make sure it is decompressed
                    UIGraphicsBeginImageContext(CGSizeMake(100, 100));
                    [cachedImage drawAtPoint:CGPointZero];
                    UIGraphicsEndImageContext();
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self setImage:cachedImage];
                    });
                }
            }
        }] resume];
    });
    
}

- (NSURL*)imageCacheURLForFile:(NSString*)fileName {
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [cachesPath stringByAppendingPathComponent:fileName];
    return [NSURL fileURLWithPath:filePath];
}

@end
