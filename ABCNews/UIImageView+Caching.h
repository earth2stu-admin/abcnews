//
//  UIImageView+Caching.h
//  ABCNews
//
//  Created by Stu on 24/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Caching)

- (void)setImageWithURL:(NSURL*)url andPlaceholder:(UIImage*)placeholderImage;

@end
