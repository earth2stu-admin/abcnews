//
//  UIColor+ABCNews.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ABCNews)

+ (UIColor*)articleText;
+ (UIColor*)dateText;

@end
