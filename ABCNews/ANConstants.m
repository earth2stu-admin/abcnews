//
//  ANConstants.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//


NSString *const kANBaseURL = @"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=25&q=http://www.abc.net.au/news/feed/51120/rss.xml";
NSString *const kANRootObject = @"responseData";
NSString *const kANFeedObject = @"feed";
NSString *const kANEntriesArray = @"entries";
NSString *const kANFeaturedArticleIdentifier = @"FeaturedArticle";
NSString *const kANArticleIdentifier = @"Article";
