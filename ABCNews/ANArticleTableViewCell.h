//
//  ANArticleTableViewCell.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANArticle.h"

@interface ANArticleTableViewCell : UITableViewCell

- (void)setArticle:(ANArticle*)anArticle;

@end
