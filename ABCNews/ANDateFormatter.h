//
//  ANDateFormatter.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <Foundation/Foundation.h>

// use static date formatters to improve performance
@interface ANDateFormatter : NSDateFormatter

+ (ANDateFormatter*)shared;
- (NSDateFormatter*)jsonFormatter;  // used to convert from date given in json feed
- (NSDateFormatter*)cellFormatter;  // used to convert to display date for table cell

@end
