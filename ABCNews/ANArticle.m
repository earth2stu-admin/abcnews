//
//  ANArticle.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "ANArticle.h"
#import "ANDateFormatter.h"

@implementation ANArticle

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        // work out details according to https://developers.google.com/feed/v1/reference#resultJson
        self.title = [dict objectForKey:@"title"];
        self.link = [dict objectForKey:@"link"];
        self.date = [[[ANDateFormatter shared] jsonFormatter] dateFromString:[dict objectForKey:@"publishedDate"]];
        // work out image details according to http://www.rssboard.org/media-rss#media-content
        float thumbAspectRatio = 1.09f;
        float featuredAspectRatio = 1.77;
        float thumbMinWidth = 140.0f;
        float featuredMinWidth = 640.0f;
        NSDictionary *bestThumb;
        NSDictionary *bestFeatured;
        for (NSDictionary *mediaGroup in [dict objectForKey:@"mediaGroups"]) {
            for (NSDictionary *content in [mediaGroup objectForKey:@"contents"]) {
                if ([[content objectForKey:@"medium"] isEqualToString:@"image"]) {
                    for (NSDictionary *thumbnail in [content objectForKey:@"thumbnails"]) {
                        if (!bestThumb) {
                            bestThumb = thumbnail;
                            break;
                        }
                    }
                    
                    if (!bestFeatured) {
                        bestFeatured = content;
                    }
                    float width = [[content objectForKey:@"width"] floatValue];
                    float height = [[content objectForKey:@"height"] floatValue];
                    float aspect = width / height;
                    float currentThumbAspect = 0.0;
                    float currentFeaturedAspect = 0.0f;
                    if (bestThumb) {
                        currentThumbAspect = [[bestThumb objectForKey:@"width"] floatValue] / [[bestThumb objectForKey:@"height"] floatValue];
                    }
                    if (bestFeatured) {
                        currentFeaturedAspect = [[bestThumb objectForKey:@"width"] floatValue] / [[bestThumb objectForKey:@"height"] floatValue];
                    }
                    
                    if (width > thumbMinWidth && aspect <= thumbAspectRatio && aspect >= currentThumbAspect) {
                        bestThumb = content;
                    }
                    if (width > featuredMinWidth && aspect <= featuredAspectRatio && aspect >= currentFeaturedAspect) {
                        bestFeatured = content;
                    }
                }
            }
        }
        self.thumbnailURL = [bestThumb objectForKey:@"url"];
        self.featuredURL = [bestFeatured objectForKey:@"url"];
    }
    return self;
}

@end
