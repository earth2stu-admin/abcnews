//
//  UIColor+ABCNews.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "UIColor+ABCNews.h"

@implementation UIColor (ABCNews)

+ (UIColor *)articleText {
    return [UIColor colorWithRed:0.0/255.0 green:124.0/255.0 blue:196.0/255.0 alpha:1.0];
}

+ (UIColor *)dateText {
    return [UIColor colorWithWhite:0.5 alpha:1.0];
}

@end
