//
//  ANNewsTableViewController.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ANNewsTableDelegate <NSObject>

- (void)didSelectArticleWithURL:(NSURL*)url;

@end

@interface ANNewsTableViewController : UITableViewController

@property (nonatomic, weak) id<ANNewsTableDelegate> delegate;

@end
