//
//  ANConstants.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//


extern NSString *const kANBaseURL;
extern NSString *const kANRootObject;
extern NSString *const kANFeedObject;
extern NSString *const kANEntriesArray;
extern NSString *const kANFeaturedArticleIdentifier;
extern NSString *const kANArticleIdentifier;



