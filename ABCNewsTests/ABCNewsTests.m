//
//  ABCNewsTests.m
//  ABCNewsTests
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ANDateFormatter.h"

@interface ABCNewsTests : XCTestCase

@end

@implementation ABCNewsTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testJSONDateConversion {
    NSDate *convertedDate = [[[ANDateFormatter shared] jsonFormatter] dateFromString:@"Sat, 23 Aug 2014 01:56:06 -0700"];
    XCTAssert(convertedDate, @"JSON Date formatting failed");
    
    NSString *formattedDate = [[[ANDateFormatter shared] cellFormatter] stringFromDate:convertedDate];
    XCTAssert([formattedDate isEqualToString:@"Aug 23, 2014 06:56 PM"], @"Cell Date formatting failed");
    
}



@end
